---
layout: 2020/post
section: propuestas
category: talks
title: Despliegue de redes IP en emergencias con AREDN
state: cancelled
---

Charla sobre el despliegue de redes IP en emergencias mediante el uso de AREDN

## Formato de la propuesta

-   [x] &nbsp;Charla (25 minutos)
-   [ ] &nbsp;Charla relámpago (10 minutos)

## Descripción

En una emergencia las telecomunicaciones se ven afectadas:

-   Los desastres naturales tienen un poder descomunal que golpea sin miramiento a las redes de telecomunicaciones.
-   Cuando una catástrofe ocurre habitualmente los sistemas de comunicaciones se encuentran saturados y/o inestables.
-   Cada vez existe una mayor demanda de capacidades de transmisión en tiempo real para apoyar el mando y control de una emergencia.

El objetivo del proyecto [AREDN](https://www.arednmesh.org/) es brindar a los radioaficionados un medio por el que transmitir información a alta velocidad (datos y/o vídeo), que sirva para apoyar con una red TCP/IP las necesidades existentes de ancho de banda en una situación de emergencia.

El proyecto está basado en OpenWRT y [se encuentra disponible](https://github.com/aredn/aredn_ar71xx) bajo licencia libre. Con AREDN un radioaficionado colabora en desplegar una red de campo en cuestión de minutos, con enlaces de alta velocidad basados en equipos wifi de 2.4 GHz, donde los nodos vienen preconfigurados y no hace falta tener conocimientos avanzados de routing.

## Público objetivo

Cualquier curioso en general, y en particular aquellas personas susceptibles de convertirse en colaboradoras del proyecto.

## Ponente(s)

-   **Alex Casanova**: EA5HJX. Apasionado por las nuevas tecnologías, las telecomunicaciones y las emergencias. Más información en [about.me/alex.casanova](https://about.me/alex.casanova).
-   **Jesús Marín**: Ciberseguridad/Desarrollo seguro. Líder del grupo local OWASP Almería. EA7KGH. Más información en [jesusmg.org](https://www.jesusmg.org).

### Contacto(s)

-   **Jesús Marín**: contacto at jesusmg dot org

## Comentarios

De ser posible, nos gustaría disponer de un pequeño espacio adicional con una mesa y enchufe donde poder hacer demostraciones de la red a interesados.

## Condiciones

-   [x] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x] &nbsp;Al menos una persona entre los que la proponen estará presente el día programado para la charla.
