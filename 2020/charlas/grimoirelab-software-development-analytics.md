---
layout: 2020/post
section: propuestas
category: talks
title: GrimoireLab&#58 an open source platform for software development analytics
state: cancelled
---

The goal is to present GrimoireLab and how it can be easily used to extract and analyse the data of a software project. The talk
will show the overall architecture of GrimoireLab, its features and how to play with it.

## Proposal format

-   [x] &nbsp;Talk (25 minutes)
-   [ ] &nbsp;Lightning talk (10 minutes)

## Description

Nowadays software projects rely on a plethora of tools and platforms for their daily activities. For instance, it's common to use Git for developing code, GitHub or GitLab to track bugs, Slack or Mattermost for instant messaging, StackOverflow or Discourse as Q&A forums, Dockerhub to share containerized versions of the software, and so on. Although these tools play a key role for the project, they contribute to scatter the project data, limiting the global understanding of the project, and the comparison with other projects.

GrimoireLab is a tool that allows to fetch, process and visualize data for more than 30 tools and platforms used in software development. Furthermore, it provides insights about the source code (e.g., complexity, licenses and dependencies) and sentiment analysis as well as a useful mechanism to unify the contributions of the same user, who may use different emails, usernames in the project.

GrimoireLab leverages on the ElasticSearch ecosystem and it's part of CHAOSS, a Linux Foundation project focused on creating analytics and metrics to help define community health.

This talk will go through the GrimoireLab architecture and show how easy is to analyse a software project.

## Target audience

-   End users interested in analyzing their software projects.
-   Developers interested in ETL processes, Python and the ElasticSearch ecosystem.
-   Reseachers interested in software mining.

## Speaker(s)

-   **Valerio Cosentino**:
    -   I'm a software engineer at Bitergia, and one of the maintainers of GrimoireLab. My interests cover source code analysis, (software) data extraction, reverse engineering and open source.
-   **Quan Zhou**:
    -   I am a DevOps engineer at Bitergia who loves to automate boring and time-consuming tasks. I love Python, MySQL and ElasticSearch technologies.

### Contact(s)

-   **Valerio Cosentino**: valcos at bitergia dot com | [@valeriocos](https://github.com/valeriocos) (GitHub) | [@_valcos_](https://twitter.com/_valcos_) (Twitter)
-   **Quan Zhou**: quan at bitergia dot com | [@zhquan](https://github.com/zhquan) (GitHub) | [@zh_quan](https://twitter.com/zh_quan) (Twitter)

## Observations

None.

## Conditions

-   [x] &nbsp;I agree to follow the [code of conduct](https://eslib.re/conducta/) and request this acceptance from the attendees and speakers.
-   [x] &nbsp;At least one person among those proposing will be present on the day scheduled for the talk.
